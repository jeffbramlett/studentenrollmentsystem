﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Jbramlett.StudentEnrollment.Data;
using Jbramlett.StudentEnrollment.DynamoBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jbramlett.StudentEnrollment.Lambda.Enroll.Repository
{
    public class EnrollmentRepository : AbstractDynamoClient<EnrollmentInfo>, IEnrollmentRepository
    {
        public EnrollmentRepository(string endpointUrl, string tablename, KeyValuePair<string, ScalarAttributeType> hashPair, KeyValuePair<string, ScalarAttributeType> rangePair, int writeUnits = 1, int readUnits = 1, bool useLocal = true) :
            base(endpointUrl, tablename, hashPair, rangePair, writeUnits, readUnits, useLocal)
        {

        }
        public async Task<bool> AddEnrollment(EnrollmentInfo info)
        {
            return await PutIntoTable(info);
        }

        public async Task<EnrollmentInfo> GetAllEnrollmentForStudent(string studentemailId)
        {
            return await GetFromTable(studentemailId, "");
        }

        public async Task<bool> UpdateEnrollmentForStudent(string previousCourseId, EnrollmentInfo info)
        {
            await DeleteEnrollment(new EnrollmentInfo() { StudentEmailId = info.StudentEmailId, CourseId = previousCourseId });
            return await AddEnrollment(info);
        }

        public async Task<bool> DeleteEnrollment(EnrollmentInfo info)
        {
            return await DeleteFromTable(info.StudentEmailId, info.CourseId);
        }

        public async Task<bool> DeleteTable()
        {
            return await DeleteTable(true);
        }

        protected override void CompleteQueryRequest(QueryRequest request, string key, string range)
        {
            request.ExpressionAttributeNames = new Dictionary<string, string>()
            {
                { "#em", "StudentEmailId" }
            };

            request.ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
            {
                {":id", new AttributeValue() {S = key } }
            };

            request.KeyConditionExpression = "#em = :id";
        }

        protected override void CompleteUpdateRequest(UpdateItemRequest request, EnrollmentInfo item)
        {
        }

        protected override EnrollmentInfo ItemFromDynamo(Dictionary<string, AttributeValue> itemFromDynamo)
        {
            EnrollmentInfo info = new EnrollmentInfo();

            info.CourseId = itemFromDynamo["CourseId"].S;
            info.StudentEmailId = itemFromDynamo["StudentEmailId"].S;

            return info;
        }
    }
}
