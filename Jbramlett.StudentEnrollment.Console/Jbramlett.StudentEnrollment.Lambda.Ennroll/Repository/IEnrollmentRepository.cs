﻿using Jbramlett.StudentEnrollment.Data;
using System.Threading.Tasks;

namespace Jbramlett.StudentEnrollment.Lambda.Enroll.Repository
{
    public interface IEnrollmentRepository
    {
        Task<bool> AddEnrollment(EnrollmentInfo info);
        Task<bool> DeleteEnrollment(EnrollmentInfo info);
        Task<bool> DeleteTable();
        Task<EnrollmentInfo> GetAllEnrollmentForStudent(string studentemailId);
        Task<bool> UpdateEnrollmentForStudent(string previousCourseId, EnrollmentInfo info);
    }
}