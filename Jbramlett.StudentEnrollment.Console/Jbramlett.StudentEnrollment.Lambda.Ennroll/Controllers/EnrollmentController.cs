using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Standard.Extensions;
using Jbramlett.StudentEnrollment.Data;
using Jbramlett.StudentEnrollment.Lambda.Enroll.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Jbramlett.StudentEnrollment.Lambda.Ennroll.Controllers
{
    [Route("api/[controller]")]
    public class EnrollmentController : ControllerBase
    {
        EnrollmentRepository _enrollmentRepo;

        private EnrollmentRepository EnrollmentRepo
        {
            get
            {
                if (_enrollmentRepo == null)
                {
                    var env = Startup.Configuration["ASPNETCORE_ENVIRONMENT"];
                    var baseurl = env.ToLower() == "release" ? "" : "http://localhost:8000";

                    _enrollmentRepo = new EnrollmentRepository(baseurl, "Jbramlett.Enrollments",
                        new KeyValuePair<string, Amazon.DynamoDBv2.ScalarAttributeType>("StudentEmailId", Amazon.DynamoDBv2.ScalarAttributeType.S),
                        new KeyValuePair<string, Amazon.DynamoDBv2.ScalarAttributeType>("CourseId", Amazon.DynamoDBv2.ScalarAttributeType.S));
                    
                }

                return _enrollmentRepo;
            }
        }
        [HttpGet]
        public EnrollmentInfo Get([FromQuery] string emailId)
        {
            return EnrollmentRepo.GetAllEnrollmentForStudent(emailId).Result;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] EnrollmentInfo enrollmentInfo)
        {
            var validation = enrollmentInfo.Validate();

            if(validation != null)
            {
                Response.StatusCode = StatusCodes.Status400BadRequest;
                var validationAsJson = validation.ToJson();
                Response.WriteAsync(validationAsJson).Wait();
                return;
            }
            var success = EnrollmentRepo.AddEnrollment(enrollmentInfo).Result;
            if (!success)
                Response.StatusCode = StatusCodes.Status500InternalServerError;
        }

        [HttpPut("{id}")]
        public void Put(string id, [FromBody] EnrollmentInfo enrollmentInfo)
        {
            var validation = enrollmentInfo.Validate();

            if (validation != null)
            {
                Response.StatusCode = StatusCodes.Status400BadRequest;
                var validationAsJson = validation.ToJson();
                Response.WriteAsync(validationAsJson).Wait();
                return;
            }

            var success = EnrollmentRepo.UpdateEnrollmentForStudent(id, enrollmentInfo).Result;
            if (!success)
                Response.StatusCode = StatusCodes.Status500InternalServerError;
        }

        [HttpDelete("{studentId}")]
        public void Delete(string studentId)
        {
            var enrolled = EnrollmentRepo.GetAllEnrollmentForStudent(studentId).Result;

            if(enrolled != null)
            {
                var success = EnrollmentRepo.DeleteEnrollment(enrolled).Result;
                if (!success)
                    Response.StatusCode = StatusCodes.Status500InternalServerError;
            }
            else
            {
                Response.StatusCode = StatusCodes.Status400BadRequest;
            }
        }

        [HttpDelete("table")]
        public void Delete()
        {
            var success = EnrollmentRepo.DeleteTable().Result;
            if (!success)
                Response.StatusCode = StatusCodes.Status500InternalServerError;
        }
    }
}
