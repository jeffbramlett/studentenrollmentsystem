﻿using Jbramlett.StudentEnrollment.Data;

namespace Jbramlett.StudentEnrollment.Console
{
    interface IEnrollmentServiceClient
    {
        bool EnrollStudentInCourse(Student student, Course course);
        EnrollmentData GetStudentEnrollment(Student student);
        bool UpdateStudentEnrollment(Student student, string previousId, Course course);

        bool DeleteEnrollment(Student student);
        void ClearTable();
    }
}