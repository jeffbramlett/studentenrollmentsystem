﻿using Common.AsyncLogging;
using Common.Standard;
using Jbramlett.StudentEnrollment.Data;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Jbramlett.StudentEnrollment.Console
{
    /// <summary>
    /// Executable class for User interaction with console
    /// </summary>
    class Program
    {
        #region Static Fields
        private static DefaultFileLog _logFile;
        private static IStudentServiceClient _studentServiceClient;
        private static ICourseServiceClient _courseServiceClient;
        private static IEnrollmentServiceClient _enrollServiceClient;
        private static string Spacer = "--------------------------------------------------------------------------------";
        #endregion

        #region Properties
        private static IConfigurationRoot Config { get; set; }

        private static DefaultFileLog LogFile
        {
            get
            {
                _logFile = _logFile ?? new DefaultFileLog();
                return _logFile;
            }
        }

        private static IStudentServiceClient StudentServiceClient
        {
            get
            {
                if(_studentServiceClient ==  null)
                {
                    _studentServiceClient = new StudentServiceClient(Config["StudentServiceUrl"]);
                }

                return _studentServiceClient;
            }
        }

        private static ICourseServiceClient CourseServiceClient
        {
            get
            {
                if(_courseServiceClient == null)
                {
                    _courseServiceClient = new CourseServiceClient(Config["CourseServiceUrl"]);
                }

                return _courseServiceClient;
            }
        }

        private static IEnrollmentServiceClient EnrollmentServiceClient
        {
            get
            {
                if(_enrollServiceClient == null)
                {
                    _enrollServiceClient = new EnrollmentServiceClient(Config["EnrollmentServiceUrl"], CourseServiceClient);
                }

                return _enrollServiceClient;
            }
        }
        #endregion

        #region EntryPoint
        static void Main(string[] args)
        {
            SetupLogging();
            CommonLogger.Instance.LogDebug(typeof(Program), "Starting Application");

            try
            {

                ConsoleWriter.Instance.WriteInformation("Student Enrollment System by Jeff Bramlett");
                ConsoleWriter.Instance.WriteInformation("");

                GetConfiguration();
                CommonLogger.Instance.LogInfo(typeof(Program), $"Settings:CourseServiceUrl = {Config["CourseServiceUrl"]}");
                CommonLogger.Instance.LogInfo(typeof(Program), $"Settings:EnrollmentServiceUrl = {Config["EnrollmentServiceUrl"]}");
                CommonLogger.Instance.LogInfo(typeof(Program), $"Settings:StudentServiceUrl = {Config["StudentServiceUrl"]}");

                bool exitMenu = false;

                while (!exitMenu)
                {
                    var courseMenuSelected = GetCourseMenuSelection();
                    CommonLogger.Instance.LogInfo(typeof(Program), $"User selected course command {courseMenuSelected} from menu");

                    switch (courseMenuSelected)
                    {
                        case 1:
                            var courseSeleced = GetCourseSelection();
                            if (courseSeleced != null)
                            {
                                CommonLogger.Instance.LogInfo(typeof(Program), $"User selected course {courseSeleced}");
                                EnrollStudent(courseSeleced);
                            }
                            break;
                        case 2:
                            ViewEnrollment();
                            break;
                        case 3:
                            UpdateEnrollment();
                            break;
                        case 4:
                            DeleteEnrollment();
                            break;
                        case 5:
                            ClearDatabase();
                            break;
                        default:
                            exitMenu = true;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonLogger.Instance.LogFatal(typeof(Program), ex.Message);
                ConsoleWriter.Instance.WriteWarning(ex.Message);
            }

            ConsoleWriter.Instance.WriteInformation("");
            ConsoleWriter.Instance.WriteInformation("Press the Q key to quit", true, ConsoleKey.Q);
        }
        #endregion

        #region Configuration
        static void GetConfiguration()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env}.json", true, true)
                .AddEnvironmentVariables();

            Config = builder.Build();
        }

        static void SetupLogging()
        {
            CommonLogger.Instance.SetWriteAction(WriteLogData);
        }

        static void WriteLogData(LogData logData)
        {
            LogFile.WriteToLog(logData.ToKeyValuePairs());
        }
        #endregion

        #region Commands
        static void EnrollStudent(Course course)
        {
            CommonLogger.Instance.LogInfo(typeof(Program), $"Enrolling student in course {course}");
            Student student;
            if (GetStudent(out student))
            {
                var success = EnrollmentServiceClient.EnrollStudentInCourse(student, course);
                if (success)
                    ShowStudentEnrollment(student);
                else
                    ConsoleWriter.Instance.WriteWarning($"Could not enroll student {student.EmailId}");
            }
            else
            {
                ConsoleWriter.Instance.WriteWarning($"Cannot find student");
            }
        }

        static void UpdateEnrollment()
        {
            CommonLogger.Instance.LogInfo(typeof(Program), $"Update Enrollment");
            Student student;
            if (GetStudent(out student))
            {
                var currentEnrollment = EnrollmentServiceClient.GetStudentEnrollment(student);

                if(currentEnrollment == null)
                {
                    ConsoleWriter.Instance.WriteImportant($"No current enrollment found for {student.EmailId}");
                    return;
                }
                var courseSeleced = GetCourseSelection(currentEnrollment.Course);
                if (courseSeleced != null)
                {
                    CommonLogger.Instance.LogInfo(typeof(Program), $"User selected course {courseSeleced}");
                    EnrollmentServiceClient.UpdateStudentEnrollment(student, currentEnrollment.Course.CourseId, courseSeleced);
                    ShowStudentEnrollment(student);
                }
            }
            else
            {
                ConsoleWriter.Instance.WriteWarning($"Cannot find student");
            }
        }

        static void DeleteEnrollment()
        {
            CommonLogger.Instance.LogInfo(typeof(Program), $"Delete Enrollment");
            Student student;
            if (GetStudent(out student))
            {
                var success = EnrollmentServiceClient.DeleteEnrollment(student);
                if(!success)
                {
                    ConsoleWriter.Instance.WriteEmphasis($"Unable to delete enrollment for {student.EmailId}");
                }
            }
            else
            {
                ConsoleWriter.Instance.WriteWarning($"Cannot find student");
            }
        }

        static void ViewEnrollment()
        {
            CommonLogger.Instance.LogInfo(typeof(Program), $"View Enrollment");
            Student student;
            if (GetStudent(out student))
            {
                ShowStudentEnrollment(student);

            }
            else
            {
                ConsoleWriter.Instance.WriteWarning($"Cannot find student");
            }
        }

        static void ClearDatabase()
        {
            CommonLogger.Instance.LogInfo(typeof(Program), $"Clearing database");

            EnrollmentServiceClient.ClearTable();
            ConsoleWriter.Instance.WriteInformation("Cleared Enrollments");

            StudentServiceClient.ClearTable();
            ConsoleWriter.Instance.WriteInformation("Cleared Students");

            CourseServiceClient.ClearTable();
            ConsoleWriter.Instance.WriteInformation("Cleared Courses");
        }
        #endregion

        #region Courses
        static int GetCourseMenuSelection()
        {
            ConsoleWriter.Instance.WriteInformation("");
            var courseMenuSelection = GetMenuSelection("Course Menu", "Course Enrollment", "View Enrollment", "Update Enrollment", "Delete Enrollment","Reset data");

            return courseMenuSelection;
        }

        static Course GetCourseSelection(Course currentCourse = null)
        {
            var courses = CourseServiceClient.GetAllCourses();
            if (currentCourse != null)
            {
                var ndx = courses.FindIndex(c => c.CourseId == currentCourse.CourseId);
                courses.RemoveAt(ndx);
            }

            if (courses != null)
            {
                var courseSelected = GetMenuSelection("Select Course to enroll", courses.ToArray());

                if (courseSelected == 0)
                    return null;

                return courses[courseSelected - 1];
            }
            ConsoleWriter.Instance.WriteEmphasis($"Cannot get any courses");
            return null;
        }
        #endregion

        #region Student
        static bool GetStudent(out Student student)
        {
            student = null;

            var emailId = "";
            if(!GetInput("Enter student email address", ref emailId))
            {
                return false;
            }

            if (!Utilities.ValidateEmail(emailId))
            {
                ConsoleWriter.Instance.WriteWarning($"The email address entered ({emailId}) is not valid");
                return false;
            }

            if (!StudentServiceClient.AcquireStudent(emailId, out student))
            {
                Student studentInput;
                if (GetInputForNewStudent(out studentInput))
                {
                    if (StudentServiceClient.RegisterStudent(studentInput))
                        student = studentInput;
                }
            }

            return student != null;
        }
        static bool GetInputForNewStudent(out Student student)
        {
            student = null;

            ConsoleWriter.Instance.WriteInformation("Registering new student");

            string firstName = "";
            string lastName = "";
            string emailId = "";
            DateTime dob = DateTime.MinValue;
            string location = "";

            if (!GetInput("Enter first name of student (or a blank line to abort)", ref firstName))
            {
                return false;
            }

            if (!GetInput("Enter last name of student (or a blank line to abort)", ref lastName))
            {
                return false;
            }

            if (!GetInput("Enter email address of student (or a blank line to abort)", ref emailId))
            {
                return false;
            }

            if (!Utilities.ValidateEmail(emailId))
            {
                ConsoleWriter.Instance.WriteWarning($"The email address entered ({emailId}) is not valid");
                return false;
            }

            if (!GetDateInput("Enter birth date student (mm/dd/yyyy) (or a blank line to abort))", ref dob))
            {
                return false;
            }

            if (!GetInput("Enter location of student  (or a blank line to abort)", ref location))
            {
                return false;
            }


            student = new Student();
            student.FirstName = firstName;
            student.LastName = lastName;
            student.EmailId = emailId;
            student.DOB = dob;
            student.Location = location;

            return student != null;
        }
        #endregion

        #region Enrollment

        static void ShowStudentEnrollment(Student student)
        {
            var enrollment = EnrollmentServiceClient.GetStudentEnrollment(student);
            if (enrollment != null)
            {
                var course = CourseServiceClient.GetCourseById(enrollment.Course.CourseId);

                enrollment.Course = course;

                ConsoleWriter.Instance.WriteInformation(Spacer);
                ConsoleWriter.Instance.WriteInformation($"{enrollment}");
                ConsoleWriter.Instance.WriteInformation(Spacer);
            }
            else
            {
                ConsoleWriter.Instance.WriteEmphasis($"No enrollment found for {student.EmailId}");
            }
        }
        #endregion

        #region Getting Input
        static bool GetInput(string question, ref string output)
        {
            ConsoleWriter.Instance.WriteImportant(question);
            output = System.Console.ReadLine().Trim();
            return !string.IsNullOrEmpty(output);
        }
        
        static bool GetDateInput(string question, ref DateTime date)
        {
            date = DateTime.MinValue;

            while (true)
            {
                ConsoleWriter.Instance.WriteImportant(question);
                var answer = System.Console.ReadLine().Trim();

                if (!string.IsNullOrEmpty(answer))
                {
                    if (DateTime.TryParse(answer, out date))
                    {
                        break;
                    }
                }
                else
                    break;
            }

            return date != DateTime.MinValue;
        }

        static int GetMenuSelection(string title, params object[] menuItems)
        {
            ConsoleWriter.Instance.WriteImportant(title);
            ConsoleWriter.Instance.WriteInformation("");

            int increment = 0;
            foreach(var menuItem in menuItems)
            {
                increment++;
                ConsoleWriter.Instance.WriteImportant($"{increment}. {menuItem}");
            }

            increment++;
            ConsoleWriter.Instance.WriteImportant($"{increment}. Exit");

            ConsoleWriter.Instance.WriteImportant("");
            ConsoleWriter.Instance.WriteImportant("Press the number of your selection");

            int choice = 0;
            while (true)
            {
                var inputKey = System.Console.ReadKey();
                System.Console.Write("\b \b"); // erase key

                char[] keyArr = { inputKey.KeyChar };
                var input = new string(keyArr);

                int inputAsInt;
                if (int.TryParse(input, out inputAsInt))
                {
                    if (inputAsInt >= 1 && inputAsInt <= menuItems.Length + 1)
                    {
                        choice = inputAsInt;
                        break;
                    }
                    else
                        break;
                }
            }

            return choice;
        }
        #endregion
    }
}
