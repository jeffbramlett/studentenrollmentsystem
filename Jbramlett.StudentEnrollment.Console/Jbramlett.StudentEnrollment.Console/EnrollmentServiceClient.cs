﻿using Jbramlett.StudentEnrollment.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Jbramlett.StudentEnrollment.Console
{
    class EnrollmentServiceClient : AbstractServiceClient<EnrollmentInfo>, IEnrollmentServiceClient
    {
        public ICourseServiceClient CourseService { get; set; }

        public EnrollmentServiceClient(string baseUrl, ICourseServiceClient courseSrv) : base(baseUrl)
        {
            CourseService = courseSrv;
        }

        public bool HasEnrollment(Student student)
        {
            var uri = $"{BaseUrl}api/enrollment?emailId={student.EmailId}";
            var response = GetItem(uri).Result;

            if(response.IsSuccessStatusCode)
            {
                var info = ConvertContent(response).Result;

                return info != null;
            }
            else
            {
                LogUnsuccessfulResponse(response);

                return false;
            }
        }

        public bool EnrollStudentInCourse(Student student, Course course)
        {
            if (HasEnrollment(student))
            {
                return false;
            }
            var uri = $"{BaseUrl}api/enrollment";
            EnrollmentInfo info = new EnrollmentInfo() { StudentEmailId = student.EmailId, CourseId = course.CourseId };
            var response = Post(uri, info).Result;

            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                LogUnsuccessfulResponse(response);

                return false;
            }
        }

        public bool UpdateStudentEnrollment(Student student, string previousId, Course course)
        {
            var uri = $"{BaseUrl}api/enrollment/{previousId}";
            EnrollmentInfo info = new EnrollmentInfo() { StudentEmailId = student.EmailId, CourseId = course.CourseId };
            var response = Put(uri, info).Result;


            return response.IsSuccessStatusCode;
        }

        public bool DeleteEnrollment(Student student)
        {
            var uri = $"{BaseUrl}api/enrollment/{student.EmailId}";

            var response = Delete(uri).Result;

            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                LogUnsuccessfulResponse(response);

                return false;
            }
        }

        public EnrollmentData GetStudentEnrollment(Student student)
        {
            var uri = $"{BaseUrl}api/enrollment?emailId={student.EmailId}";
            var response = GetItem(uri).Result;

            if (response.IsSuccessStatusCode)
            {
                var enrollmentInfo = ConvertContent(response).Result;

                if (enrollmentInfo != null)
                {
                    var course = CourseService.GetCourseById(enrollmentInfo.CourseId);

                    EnrollmentData ed = new EnrollmentData() { Student = student, Course = course };

                    return ed;
                }
            }
            else
            {
                LogUnsuccessfulResponse(response);
            }
            
            return null;
        }

        public void ClearTable()
        {
            var requestUri = $"{BaseUrl}api/enrollment/table";
            var response = ClearTable(requestUri).Result;
            if (!response.IsSuccessStatusCode)
            {
                LogUnsuccessfulResponse(response);
            }
        }
    }
}
