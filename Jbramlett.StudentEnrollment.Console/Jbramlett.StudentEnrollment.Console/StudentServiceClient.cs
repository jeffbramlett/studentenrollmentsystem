﻿using Jbramlett.StudentEnrollment.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Jbramlett.StudentEnrollment.Console
{
    class StudentServiceClient : AbstractServiceClient<Student>, IStudentServiceClient
    {
        public StudentServiceClient(string baseUrl) : base(baseUrl)
        {

        }

        public bool RegisterStudent(Student student)
        {
            var uri = $"{BaseUrl}api/student";
            var response = Post(uri, student).Result;

            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                LogUnsuccessfulResponse(response);

                return false;
            }
        }

        public bool AcquireStudent(string emailId, out Student student)
        {
            student = null;

            var uri = $"{BaseUrl}api/student?emailId={emailId}";
            var response = GetItem(uri).Result;

            if (response.IsSuccessStatusCode)
            {
                student = ConvertContent(response).Result;
            }
            else
            {
                LogUnsuccessfulResponse(response);
            }

            return student != null;
        }

        public void ClearTable()
        {
            var requestUri = $"{BaseUrl}api/student/table";
            var response = ClearTable(requestUri).Result;
            if (!response.IsSuccessStatusCode)
            {
                LogUnsuccessfulResponse(response);
            }
        }
    }
}
