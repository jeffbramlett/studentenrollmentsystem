﻿using Jbramlett.StudentEnrollment.Data;

namespace Jbramlett.StudentEnrollment.Console
{
    interface IStudentServiceClient
    {
        bool AcquireStudent(string emailId, out Student student);
        bool RegisterStudent(Student student);
        void ClearTable();
    }
}