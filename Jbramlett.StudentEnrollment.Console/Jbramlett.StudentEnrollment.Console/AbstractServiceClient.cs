﻿using Common.AsyncLogging;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Jbramlett.StudentEnrollment.Console
{
    abstract class AbstractServiceClient<T> where T:class
    {
        public string BaseUrl { get; set; }

        public AbstractServiceClient(string baseUrl)
        {
            BaseUrl = baseUrl;
        }

        protected async Task<HttpResponseMessage> GetArray(string uri)
        {
            var client = new HttpClient();
            return await client.GetAsync(uri);
        }

        protected async Task<HttpResponseMessage> GetItem(string uri)
        {
            var client = new HttpClient();
            return await client.GetAsync(uri);
        }

        protected async Task<HttpResponseMessage> Post(string uri, T itemToPost)
        {
            var client = new HttpClient();
            var inputContent = new StringContent(JsonConvert.SerializeObject(itemToPost), Encoding.UTF8, "application/json");
            return await client.PostAsync(uri, inputContent);
        }

        protected async Task<HttpResponseMessage> Put(string uri, T itemToPut)
        {
            var client = new HttpClient();
            var inputContent = new StringContent(JsonConvert.SerializeObject(itemToPut), Encoding.UTF8, "application/json");
            return await client.PutAsync(uri, inputContent);
        }

        protected async Task<HttpResponseMessage> Delete(string uri)
        {
            var client = new HttpClient();
            return await client.DeleteAsync(uri);
        }

        protected async Task<HttpResponseMessage> ClearTable(string uri)
        {
            var client = new HttpClient();
            return await client.DeleteAsync(uri);
        }

        protected async Task<T> ConvertContent(HttpResponseMessage response)
        {
            if(response.IsSuccessStatusCode)
            {
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = await response.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<T>(responseContent);
                }
            }
            return null;
        }
        protected async Task<List<T>> ConvertContentArray(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
            {
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = await response.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<List<T>>(responseContent);
                }
            }
            return null;
        }

        protected void LogUnsuccessfulResponse(HttpResponseMessage response)
        {
            switch (response.StatusCode)
            {
                case System.Net.HttpStatusCode.BadRequest:
                    var content = response.Content.ReadAsStringAsync().Result;
                    CommonLogger.Instance.LogWarning(GetType(), message: $"Response for {response.RequestMessage.RequestUri} failed with status code {response.StatusCode}: {response.ReasonPhrase}\n{content}");
                    break;
                // TODO: Add more as needed to address specific status codes differently
                default:
                    CommonLogger.Instance.LogWarning(GetType(), message: $"Response for {response.RequestMessage.RequestUri} failed with status code {response.StatusCode}: {response.ReasonPhrase}");
                    break;
            }
        }

    }
}
