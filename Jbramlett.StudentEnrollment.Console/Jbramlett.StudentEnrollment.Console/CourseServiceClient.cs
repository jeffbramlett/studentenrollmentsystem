﻿using Common.AsyncLogging;
using Jbramlett.StudentEnrollment.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Jbramlett.StudentEnrollment.Console
{
    class CourseServiceClient : AbstractServiceClient<Course>, ICourseServiceClient
    {

        public CourseServiceClient(string baseUrl) : base(baseUrl)
        {
        }

        public Course GetCourseById(string courseId)
        {
            var requestUri = $"{BaseUrl}api/Courses/{courseId}";

            var response = GetItem(requestUri).Result;

            if(response.IsSuccessStatusCode)
                return ConvertContent(response).Result;

            else
            {
                LogUnsuccessfulResponse(response);

                return null;
            }
        }

        public List<Course> GetAllCourses()
        {
            var requestUri = $"{BaseUrl}api/Courses";

            var response = GetArray(requestUri).Result;
            if (response.IsSuccessStatusCode)
            {
                return ConvertContentArray(response).Result;
            }
            else
            {
                LogUnsuccessfulResponse(response);

                return null;
            }
        }

        public void ClearTable()
        {
            var requestUri = $"{BaseUrl}api/Courses/table";
            var response = ClearTable(requestUri).Result;

            if (!response.IsSuccessStatusCode)
            {
                LogUnsuccessfulResponse(response);
            }
        }
    }
}
