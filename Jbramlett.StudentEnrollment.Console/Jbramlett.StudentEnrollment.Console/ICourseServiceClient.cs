﻿using Jbramlett.StudentEnrollment.Data;
using System.Collections.Generic;

namespace Jbramlett.StudentEnrollment.Console
{
    interface ICourseServiceClient
    {
        List<Course> GetAllCourses();

        Course GetCourseById(string courseId);

        void ClearTable();
    }
}