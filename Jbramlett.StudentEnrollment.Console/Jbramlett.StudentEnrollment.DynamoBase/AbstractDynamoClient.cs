﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Common.AsyncLogging;
using Common.Standard.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jbramlett.StudentEnrollment.DynamoBase
{
    public abstract class AbstractDynamoClient<T> where T:class
    {
        #region Fields
        private AmazonDynamoDBClient _amazonDbClient;
        private AmazonDynamoDBConfig _amazonDbConfig;
        private List<AttributeDefinition> _tableAttributes;
        private List<KeySchemaElement> _tableKeySchema;
        private ProvisionedThroughput _provisionedThroughput;
        #endregion

        #region Properties
        private string EndPointUrl { get; set; }

        protected string TableName { get; set; }

        private string KeyName { get; set; }

        private string RangeName { get; set; }

        private List<AttributeDefinition> TableAttributes
        {
            get { _tableAttributes = _tableAttributes ?? new List<AttributeDefinition>(); return _tableAttributes; }
        }

        private List<KeySchemaElement> KeySchemas
        {
            get { _tableKeySchema = _tableKeySchema ?? new List<KeySchemaElement>(); return _tableKeySchema; }
        }

        private ProvisionedThroughput ProvisionedThroughput
        {
            get { _provisionedThroughput = _provisionedThroughput ?? new ProvisionedThroughput(); return _provisionedThroughput; }
        }

        private AmazonDynamoDBConfig AmazonDbConfig
        {
            get
            {
                if (_amazonDbConfig == null)
                {
                    _amazonDbConfig = new AmazonDynamoDBConfig();
                    _amazonDbConfig.ServiceURL = EndPointUrl;
                }

                return _amazonDbConfig;
            }
        }
        private AmazonDynamoDBClient AmazonDbClient
        {
            get
            {
                if (_amazonDbClient == null)
                {
                    Console.WriteLine($"Endpoint={EndPointUrl}");

                    if (!string.IsNullOrEmpty(EndPointUrl))
                    {
                        _amazonDbClient = new AmazonDynamoDBClient(AmazonDbConfig);
                    }
                    else
                    {
                        _amazonDbClient = new AmazonDynamoDBClient();
                    }
                }
                return _amazonDbClient;
            }
        }
        #endregion

        #region Ctors and Dtors
        public AbstractDynamoClient(string endpointUrl, string tablename, KeyValuePair<string, ScalarAttributeType> hashPair, KeyValuePair<string, ScalarAttributeType> rangePair, int writeUnits = 1, int readUnits = 1, bool useLocal = true)
        {
            CommonLogger.Instance.LogDebug(GetType(), $"endpointUrl={endpointUrl}, tablename={tablename}");

            TableName = string.IsNullOrEmpty(tablename) ? typeof(T).Name : tablename;

            hashPair.ThrowIfNull();
            rangePair.ThrowIfNull();

            KeyName = hashPair.Key;
            RangeName = rangePair.Key;

            EndPointUrl = endpointUrl;

            TableAttributes.Add(new AttributeDefinition(hashPair.Key, hashPair.Value));
            TableAttributes.Add(new AttributeDefinition(rangePair.Key, rangePair.Value));

            KeySchemas.Add(new KeySchemaElement(hashPair.Key, "HASH"));
            KeySchemas.Add(new KeySchemaElement(rangePair.Key, "RANGE"));

            ProvisionedThroughput.ReadCapacityUnits = readUnits;
            ProvisionedThroughput.WriteCapacityUnits = writeUnits;
        }
        #endregion

        #region Protected Table Commands

        private async Task<Table> AssureTable()
        {
            CommonLogger.Instance.LogDebug(GetType(), message: $"Assuring Table {TableName}");

            var tableExists = await DoesTableExist();

            CommonLogger.Instance.LogDebug(GetType(), message: $"Table {TableName} exists? {tableExists}");
            if (!tableExists)
            {
                if (!await CreateTable())
                    return null;
            }

            return Table.LoadTable(AmazonDbClient, TableName);
        }

        protected async Task<bool> DoesTableExist()
        {
            try
            {
                ListTablesResponse tblResponse = await AmazonDbClient.ListTablesAsync();

                return tblResponse.TableNames.Contains(TableName);
            }
            catch (Exception ex)
            {
                CommonLogger.Instance.LogError(GetType(), message: ex.Message, ex: ex);
                return false;
            }
        }

        private async Task<bool> CreateTable()
        {
            try
            {
                CreateTableRequest createRequest = new CreateTableRequest(TableName, KeySchemas, TableAttributes, ProvisionedThroughput);
                var response = await AmazonDbClient.CreateTableAsync(createRequest);

                await Task.Delay(5000);

                var desc = AmazonDbClient.DescribeTableAsync(TableName).Result;


                CommonLogger.Instance.LogDebug(GetType(), message: desc.ToJson());

                return response.HttpStatusCode == System.Net.HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                CommonLogger.Instance.LogError(GetType(), message: ex.Message, ex: ex);
                return false;
            }
        }
        #endregion

        #region Abstracts and Virtuals
        protected abstract T ItemFromDynamo(Dictionary<string, AttributeValue> itemFromDynamo);
        protected abstract void CompleteUpdateRequest(UpdateItemRequest request, T item);
        protected abstract void CompleteQueryRequest(QueryRequest request, string key, string range);
        #endregion

        #region Find
        protected async Task<IEnumerable<T>> GetEnumeration()
        {
            List<T> items = new List<T>();

            try
            {
                var table = await AssureTable();

                if (table != null)
                {
                    ScanRequest scanReq = new ScanRequest()
                    {
                        TableName = TableName
                    };
                    var result = await AmazonDbClient.ScanAsync(scanReq);

                    foreach (Dictionary<string, AttributeValue> item in result.Items)
                    {
                        items.Add(ItemFromDynamo(item));
                    }

                    items.Sort();
                }
                else
                {
                    CommonLogger.Instance.LogWarning(GetType(), $"Cannot assure table {TableName}");
                }
            }
            catch (Exception ex)
            {
                CommonLogger.Instance.LogError(GetType(), message: ex.Message, ex: ex);
            }

            return items;
        }

        protected async Task<T> GetFromTable(string key, string range)
        {
            try
            {
                await AssureTable();

                QueryRequest qRequest = new QueryRequest
                {
                    TableName = TableName
                };
                CompleteQueryRequest(qRequest, key, range);

                var response = await AmazonDbClient.QueryAsync(qRequest);

                if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    foreach (var item in response.Items)
                    {
                        return ItemFromDynamo(item);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonLogger.Instance.LogError(GetType(), message: ex.Message, ex: ex);
            }

            return null;
        }

   
        #endregion

        #region Put
        protected async Task<bool> PutIntoTable( T itemToPut)
        {
            try
            {
                var table = await AssureTable();
            
                var asJson = itemToPut.ToJson();
                Document doc = Document.FromJson(asJson);
                await table.PutItemAsync(doc);

                return true;
            }
            catch (Exception ex)
            {
                CommonLogger.Instance.LogError(GetType(), message: ex.Message, ex: ex);
                return false;
            }

        }

        #endregion

        #region Update
        protected async Task<bool> UpdateInTable(string key, string range, T itemToUpdate)
        {
            try
            {
                await AssureTable();

                UpdateItemRequest updateReq = new UpdateItemRequest()
                {
                    TableName = TableName,
                    Key = new Dictionary<string, AttributeValue>
                    {
                      { KeyName, new AttributeValue { S = key } },
                      { RangeName, new AttributeValue { S = range } }
                    }
                };

                CompleteUpdateRequest(updateReq, itemToUpdate);

                var response = await AmazonDbClient.UpdateItemAsync(updateReq);

                return response.HttpStatusCode == System.Net.HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                CommonLogger.Instance.LogError(GetType(), message: ex.Message, ex: ex);
                return false;
            }
        }
        #endregion

        #region Delete
        protected async Task<bool> DeleteFromTable(string key, string range)
        {
            try
            {
                await AssureTable();

                DeleteItemRequest delReq = new DeleteItemRequest()
                {
                    TableName = TableName,
                    Key = new Dictionary<string, AttributeValue>()
                    {
                        { KeyName, new AttributeValue { S = key } },
                        { RangeName, new AttributeValue { S = range } }
                    }
                };

                var response = await AmazonDbClient.DeleteItemAsync(delReq);

                return response.HttpStatusCode == System.Net.HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                CommonLogger.Instance.LogError(GetType(), message: ex.Message, ex: ex);
                return false;
            }

        }

        protected async Task<bool> DeleteTable(bool andDeleteIt = false)
        {
            try
            {
                DeleteTableRequest delTableReq = new DeleteTableRequest()
                {
                    TableName = TableName
                };
                var response = await AmazonDbClient.DeleteTableAsync(delTableReq);

                return response.HttpStatusCode == System.Net.HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                CommonLogger.Instance.LogError(GetType(), message: ex.Message, ex: ex);
                return false;
            }
        }
        #endregion
    }
}
