using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jbramlett.StudentEnrollment.Courses.Repository;
using Jbramlett.StudentEnrollment.Data;
using Microsoft.AspNetCore.Mvc;

namespace Jbramlett.StudentEnrollment.Courses.Controllers
{
    [Route("api/[controller]")]
    public class CoursesController : ControllerBase
    {
        CourseRepository _courseRepo;

        private CourseRepository CourseRepo
        {
            get
            {
                if(_courseRepo == null)
                {
                    _courseRepo = new CourseRepository("http://localhost:8000", "Courses", 
                        new KeyValuePair<string, Amazon.DynamoDBv2.ScalarAttributeType>("CourseId",Amazon.DynamoDBv2.ScalarAttributeType.S),
                        new KeyValuePair<string, Amazon.DynamoDBv2.ScalarAttributeType>("CourseName", Amazon.DynamoDBv2.ScalarAttributeType.S));

                    var result = _courseRepo.Init().Result;
                }

                return _courseRepo;
            }
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<Course> Get()
        {
            return CourseRepo.AllCourses();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Course Get(string id)
        {
            return new Course()
            {
                CourseId = id,
                CourseName = "Course Name"
            };
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
