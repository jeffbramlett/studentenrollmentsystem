using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.AsyncLogging;
using Jbramlett.StudentEnrollment.Lambda.Student.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using data = Jbramlett.StudentEnrollment.Data;

namespace Jbramlett.StudentEnrollment.Lambda.Student.Controllers
{
    [Route("api/[controller]")]
    public class StudentController : ControllerBase
    {
        private IStudentRepository _studentRepository;

        private IStudentRepository StudentRepository
        {
            get
            {
                if(_studentRepository == null)
                {
                    var env = Startup.Configuration["ASPNETCORE_ENVIRONMENT"];
                    var baseurl = env.ToLower() == "release" ? "" : "http://localhost:8000";

                    CommonLogger.Instance.LogDebug(GetType(),message: $"Initiating Student Repository with {baseurl}");

                    _studentRepository = new StudentRepository(baseurl, "Jbramlett.Students",
                        new KeyValuePair<string, Amazon.DynamoDBv2.ScalarAttributeType>("EmailId", Amazon.DynamoDBv2.ScalarAttributeType.S),
                        new KeyValuePair<string, Amazon.DynamoDBv2.ScalarAttributeType>("Location", Amazon.DynamoDBv2.ScalarAttributeType.S));

                    CommonLogger.Instance.LogDebug(GetType(), message: $"Student Repository created for {baseurl}");
                }

                return _studentRepository;
            }
        }
        // GET api/values
        [HttpGet]
        public data.Student Get([FromQuery] string emailId)
        {
            CommonLogger.Instance.LogDebug(GetType(), message: $"Find student with emailid: {emailId}");

            data.Student student;
            if(StudentRepository.FindStudentByEmail(emailId, out student))
            {
                return student;
            }

            Response.StatusCode = StatusCodes.Status404NotFound;
            return null;
        }


        // POST api/values
        [HttpPost]
        public void Post([FromBody]data.Student student)
        {
            StudentRepository.RegisterStudent(student);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]data.Student student)
        {
        }

        // DELETE api/values/5
        [HttpDelete("table")]
        public void Delete([FromQuery] string emailId)
        {
            StudentRepository.ClearTable();
        }
    }
}
