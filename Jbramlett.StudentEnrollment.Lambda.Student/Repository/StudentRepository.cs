﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Common.AsyncLogging;
using Jbramlett.StudentEnrollment.DynamoBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using data = Jbramlett.StudentEnrollment.Data;

namespace Jbramlett.StudentEnrollment.Lambda.Student.Repository
{
    public class StudentRepository : AbstractDynamoClient<data.Student>, IStudentRepository
    {
        #region Ctors and Dtors
        public StudentRepository(string endpointUrl, string tablename, KeyValuePair<string, ScalarAttributeType> hashPair, KeyValuePair<string, ScalarAttributeType> rangePair, int writeUnits = 1, int readUnits = 1, bool useLocal = true) :
            base(endpointUrl, tablename, hashPair, rangePair, writeUnits, readUnits, useLocal)
        {

        }
        #endregion

        #region Publics
        public bool RegisterStudent(data.Student student)
        {
            var success = PutIntoTable(student).Result;
            return success;
        }

        public bool FindStudentByEmail(string emailId, out data.Student student)
        {
            student = null;
            try
            {
                student = GetFromTable(emailId, "").Result;
            }
            catch (Exception ex)
            {
                CommonLogger.Instance.LogError(GetType(), message: ex.Message, ex: ex);
            }

            return student != null;
        }

        public void ClearTable()
        {
            var success = DeleteTable(true).Result;
        }
        #endregion

        #region Overrides

        protected override data.Student ItemFromDynamo(Dictionary<string, AttributeValue> itemFromDynamo)
        {
            data.Student student = new data.Student()
            {
                EmailId = itemFromDynamo["EmailId"].S,
                FirstName = itemFromDynamo["FirstName"].S,
                LastName = itemFromDynamo["LastName"].S,
                DOB = DateTime.Parse(itemFromDynamo["DOB"].S),
                Location = itemFromDynamo["Location"].S,
            };


            return student;
        }

        protected override void CompleteUpdateRequest(UpdateItemRequest request, data.Student item)
        {
            throw new NotImplementedException();
        }

        protected override void CompleteQueryRequest(QueryRequest request, string key, string range)
        {
            request.ExpressionAttributeNames = new Dictionary<string, string>()
            {
                { "#em", "EmailId" }
            };

            request.ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
            {
                {":id", new AttributeValue() {S = key } }
            };

            request.KeyConditionExpression = "#em = :id";
        }

        #endregion
    }
}
