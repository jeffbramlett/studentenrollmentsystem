﻿namespace Jbramlett.StudentEnrollment.Lambda.Student.Repository
{
    public interface IStudentRepository
    {
        void ClearTable();
        bool FindStudentByEmail(string emailId, out Data.Student student);
        bool RegisterStudent(Data.Student student);
    }
}