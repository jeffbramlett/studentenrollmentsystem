using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jbramlett.StudentEnrollment.Lambda.Courses.Repository;
using Jbramlett.StudentEnrollment.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace Jbramlett.StudentEnrollment.Lambda.Courses.Controllers
{
    [Route("api/[controller]")]
    public class CoursesController : ControllerBase
    {
        CourseRepository _courseRepo;

        private CourseRepository CourseRepo
        {
            get
            {
                if (_courseRepo == null)
                {
                    var env = Startup.Configuration["ASPNETCORE_ENVIRONMENT"];
                    var baseurl = env.ToLower() == "release" ? "" : "http://localhost:8000";

                    _courseRepo = new CourseRepository(baseurl, "Jbramlett.Courses",
                        new KeyValuePair<string, Amazon.DynamoDBv2.ScalarAttributeType>("CourseId", Amazon.DynamoDBv2.ScalarAttributeType.S),
                        new KeyValuePair<string, Amazon.DynamoDBv2.ScalarAttributeType>("CourseName", Amazon.DynamoDBv2.ScalarAttributeType.S));

                    var result = _courseRepo.Init().Result;
                }

                return _courseRepo;
            }
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<Course> Get()
        {
            return CourseRepo.AllCourses();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Course Get(string id)
        {
            return CourseRepo.GetCourseById(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
            Response.StatusCode = StatusCodes.Status501NotImplemented;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
            Response.StatusCode = StatusCodes.Status501NotImplemented;
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Response.StatusCode = StatusCodes.Status501NotImplemented;
        }

        // DELETE api/values/5
        [HttpDelete("table")]
        public void Delete()
        {
            CourseRepo.ClearTable();
        }
    }
}
