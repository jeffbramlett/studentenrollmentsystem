﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Jbramlett.StudentEnrollment.Data;
using Jbramlett.StudentEnrollment.DynamoBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jbramlett.StudentEnrollment.Lambda.Courses.Repository
{
    public class CourseRepository : AbstractDynamoClient<Course>, ICourseRepository
    {

        public CourseRepository(string endpointUrl, string tablename, KeyValuePair<string, ScalarAttributeType> hashPair, KeyValuePair<string, ScalarAttributeType> rangePair, int writeUnits = 1, int readUnits = 1, bool useLocal = true) :
            base(endpointUrl, tablename, hashPair, rangePair, writeUnits, readUnits, useLocal)
        {

        }

        public async Task<bool> Init()
        {

            var tableExists = await DoesTableExist();
            if (!tableExists)
            {
                Course course1001 = new Course()
                {
                    CourseId = "1001",
                    CourseName = "Cloud"
                };

                Course course1002 = new Course()
                {
                    CourseId = "1002",
                    CourseName = "Java"
                };

                Course course1003 = new Course()
                {
                    CourseId = "1003",
                    CourseName = ".Net"
                };

                Course course1004 = new Course()
                {
                    CourseId = "1004",
                    CourseName = "ReactJS"
                };

                Course course1005 = new Course()
                {
                    CourseId = "1005",
                    CourseName = "Python"
                };

                await PutIntoTable(course1001);
                await PutIntoTable(course1002);
                await PutIntoTable(course1003);
                await PutIntoTable(course1004);
                await PutIntoTable(course1005);
            }
            return true;
        }

        public IEnumerable<Course> AllCourses()
        {
            return GetEnumeration().Result;
        }

        public Course GetCourseById(string courseId)
        {
            return GetFromTable(courseId, "").Result;
        }

        public bool ClearTable()
        {
            return DeleteTable(true).Result;
        }

        protected override Course ItemFromDynamo(Dictionary<string, AttributeValue> itemFromDynamo)
        {
            Course course = new Course();

            course.CourseId = itemFromDynamo["CourseId"].S;
            course.CourseName = itemFromDynamo["CourseName"].S;

            return course;
        }

        protected override void CompleteUpdateRequest(UpdateItemRequest updaterequest, Course item)
        {
            updaterequest.ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
            {
              { ":cn", new AttributeValue { S = item.CourseName } }
            };

            updaterequest.ConditionExpression = "SET CourseName= :cn";
        }

        protected override void CompleteQueryRequest(QueryRequest request, string key, string range)
        {
            request.TableName = TableName;
            request.ExpressionAttributeNames = new Dictionary<string, string>()
            {
                {"#ci", "CourseId" }
            };

            request.ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
            {
                {":ci", new AttributeValue {S = key } }
            };
            request.KeyConditionExpression = "#ci = :ci";
        }
    }
}
