﻿using Jbramlett.StudentEnrollment.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jbramlett.StudentEnrollment.Lambda.Courses.Repository
{
    public interface ICourseRepository
    {
        IEnumerable<Course> AllCourses();
        bool ClearTable();
        Course GetCourseById(string courseId);
        Task<bool> Init();
    }
}