using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jbramlett.StudentEnrollment.Data;
using Jbramlett.StudentEnrollment.Lambda.Enrollment.Repository;
using Microsoft.AspNetCore.Mvc;

namespace Jbramlett.StudentEnrollment.Lambda.Enrollment.Controllers
{
    [Route("api/[controller]")]
    public class EnrollmentController : ControllerBase
    {
        EnrollmentRepository _enrollmentRepo;

        private EnrollmentRepository EnrollmentRepo
        {
            get
            {
                if (_enrollmentRepo == null)
                {
                    var env = Startup.Configuration["ASPNETCORE_ENVIRONMENT"];
                    var baseurl = env.ToLower() == "release" ? "" : "http://localhost:8000";

                    _enrollmentRepo = new EnrollmentRepository(baseurl, "Jbramlett.Enrollments",
                        new KeyValuePair<string, Amazon.DynamoDBv2.ScalarAttributeType>("CourseId", Amazon.DynamoDBv2.ScalarAttributeType.S),
                        new KeyValuePair<string, Amazon.DynamoDBv2.ScalarAttributeType>("StudentEmail", Amazon.DynamoDBv2.ScalarAttributeType.S));

                }

                return _enrollmentRepo;
            }
        }
        [HttpGet]
        public EnrollmentInfo Get([FromQuery] string emailId)
        {
            return EnrollmentRepo.GetAllEnrollmentForStudent(emailId).Result;

        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] EnrollmentInfo enrollmentInfo)
        {
            var success = EnrollmentRepo.AddEnrollment(enrollmentInfo);
        }

        [HttpPut]
        public void Put([FromBody] EnrollmentInfo enrollmentInfo)
        {
            var success = EnrollmentRepo.UpdateEnrollmentForStudent(enrollmentInfo);
        }

        // DELETE api/values/5
        [HttpDelete]
        public void Delete([FromBody] EnrollmentInfo enrollmentInfo)
        {
            var success = EnrollmentRepo.DeleteEnrollment(enrollmentInfo);
        }
    }
}
