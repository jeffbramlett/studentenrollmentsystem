﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Jbramlett.StudentEnrollment.Data;
using Jbramlett.StudentEnrollment.DynamoBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Jbramlett.StudentEnrollment.Data;

namespace Jbramlett.StudentEnrollment.Lambda.Enrollment.Repository
{
    public class EnrollmentRepository: AbstractDynamoClient<EnrollmentInfo>
    {
        public EnrollmentRepository(string endpointUrl, string tablename, KeyValuePair<string, ScalarAttributeType> hashPair, KeyValuePair<string, ScalarAttributeType> rangePair, int writeUnits = 1, int readUnits = 1, bool useLocal = true) :
            base(endpointUrl, tablename, hashPair, rangePair, writeUnits, readUnits, useLocal)
        {

        }
        public async Task<bool> AddEnrollment(EnrollmentInfo info)
        {
            return await PutIntoTable(info);
        }

        public async Task<EnrollmentInfo> GetAllEnrollmentForStudent(string studentemailId)
        {
            return await GetFromTable(studentemailId, "");
        }

        public async Task<bool> UpdateEnrollmentForStudent(EnrollmentInfo info)
        {
            return await UpdateInTable(info.StudentEmailId, info.CourseId, info);
        }

        public async Task<bool> DeleteEnrollment(EnrollmentInfo info)
        {
            return await DeleteFromTable(info.StudentEmailId, info.CourseId);
        }

        public async Task<bool> DeleteTable()
        {
            return await DeleteTable(true);
        }

        protected override void CompleteQueryRequest(QueryRequest request, string key, string range)
        {
            throw new NotImplementedException();
        }

        protected override void CompleteUpdateRequest(UpdateItemRequest request, EnrollmentInfo item)
        {
            throw new NotImplementedException();
        }

        protected override EnrollmentInfo ItemFromDynamo(Dictionary<string, AttributeValue> itemFromDynamo)
        {
            throw new NotImplementedException();
        }
    }
}
