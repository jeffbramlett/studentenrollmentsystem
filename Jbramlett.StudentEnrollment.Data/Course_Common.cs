﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jbramlett.StudentEnrollment.Data
{
    public partial class Course: IComparable<Course>, IComparer<Course>
    {
        public int Compare(Course x, Course y)
        {
            return x.CompareTo(y);
        }

        public int CompareTo(Course other)
        {
            return this.CourseId.CompareTo(other.CourseId);
        }

        public override string ToString()
        {
            return $"{CourseId}-{CourseName}";
        }
    }
}
