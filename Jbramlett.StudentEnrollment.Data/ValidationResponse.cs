﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jbramlett.StudentEnrollment.Data
{
    public sealed class ValidationResponse
    {
        private IList<ValidationResponse> _moreResponses;

        public string Message { get; set; }
        

        public IList<ValidationResponse> MoreResponses
        {
            get
            {
                if(_moreResponses == null)
                {
                    _moreResponses = new List<ValidationResponse>();
                }

                return _moreResponses;
            }
        }
    }
}
