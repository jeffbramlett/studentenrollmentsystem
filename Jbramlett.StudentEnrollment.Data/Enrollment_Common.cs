﻿using Common.Standard.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Jbramlett.StudentEnrollment.Data
{
    public partial class EnrollmentData
    {
        public override string ToString()
        {
            var dobInFormat = Student.DOB.ToString("dd/MM/yyy");
            return $"{ Course.CourseName},{ Course.CourseId},{ Student.FirstName},{ Student.LastName},{dobInFormat},{Student.Location}";
        }
    }
}
