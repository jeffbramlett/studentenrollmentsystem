// ------------------------------------------------------------------------------
//  <auto-generated>
//    Generated by Xsd2Code. Version 3.4.0.21575 Microsoft Reciprocal License (Ms-RL) 
//    <NameSpace>Jbramlett.StudentEnrollment.Data</NameSpace><Collection>List</Collection><codeType>CSharp</codeType><EnableDataBinding>True</EnableDataBinding><EnableLazyLoading>False</EnableLazyLoading><TrackingChangesEnable>False</TrackingChangesEnable><GenTrackingClasses>False</GenTrackingClasses><HidePrivateFieldInIDE>False</HidePrivateFieldInIDE><EnableSummaryComment>False</EnableSummaryComment><VirtualProp>False</VirtualProp><IncludeSerializeMethod>False</IncludeSerializeMethod><UseBaseClass>False</UseBaseClass><GenBaseClass>False</GenBaseClass><GenerateCloneMethod>False</GenerateCloneMethod><GenerateDataContracts>False</GenerateDataContracts><CodeBaseTag>Net20</CodeBaseTag><SerializeMethodName>Serialize</SerializeMethodName><DeserializeMethodName>Deserialize</DeserializeMethodName><SaveToFileMethodName>SaveToFile</SaveToFileMethodName><LoadFromFileMethodName>LoadFromFile</LoadFromFileMethodName><GenerateXMLAttributes>False</GenerateXMLAttributes><OrderXMLAttrib>False</OrderXMLAttrib><EnableEncoding>False</EnableEncoding><AutomaticProperties>False</AutomaticProperties><GenerateShouldSerialize>False</GenerateShouldSerialize><DisableDebug>False</DisableDebug><PropNameSpecified>Default</PropNameSpecified><Encoder>UTF8</Encoder><CustomUsings></CustomUsings><ExcludeIncludedTypes>False</ExcludeIncludedTypes><EnableInitializeFields>True</EnableInitializeFields>
//  </auto-generated>
// ------------------------------------------------------------------------------
namespace Jbramlett.StudentEnrollment.Data {
    using System;
    using System.Diagnostics;
    using System.Xml.Serialization;
    using System.Collections;
    using System.Xml.Schema;
    using System.ComponentModel;
    using System.Collections.Generic;
    
    
    public partial class Student : System.ComponentModel.INotifyPropertyChanged {
        
        private string emailIdField;
        
        private string firstNameField;
        
        private string lastNameField;
        
        private string locationField;
        
        private System.DateTime dOBField;
        
        public string EmailId {
            get {
                return this.emailIdField;
            }
            set {
                if ((this.emailIdField != null)) {
                    if ((emailIdField.Equals(value) != true)) {
                        this.emailIdField = value;
                        this.OnPropertyChanged("EmailId");
                    }
                }
                else {
                    this.emailIdField = value;
                    this.OnPropertyChanged("EmailId");
                }
            }
        }
        
        public string FirstName {
            get {
                return this.firstNameField;
            }
            set {
                if ((this.firstNameField != null)) {
                    if ((firstNameField.Equals(value) != true)) {
                        this.firstNameField = value;
                        this.OnPropertyChanged("FirstName");
                    }
                }
                else {
                    this.firstNameField = value;
                    this.OnPropertyChanged("FirstName");
                }
            }
        }
        
        public string LastName {
            get {
                return this.lastNameField;
            }
            set {
                if ((this.lastNameField != null)) {
                    if ((lastNameField.Equals(value) != true)) {
                        this.lastNameField = value;
                        this.OnPropertyChanged("LastName");
                    }
                }
                else {
                    this.lastNameField = value;
                    this.OnPropertyChanged("LastName");
                }
            }
        }
        
        public string Location {
            get {
                return this.locationField;
            }
            set {
                if ((this.locationField != null)) {
                    if ((locationField.Equals(value) != true)) {
                        this.locationField = value;
                        this.OnPropertyChanged("Location");
                    }
                }
                else {
                    this.locationField = value;
                    this.OnPropertyChanged("Location");
                }
            }
        }
        
        public System.DateTime DOB {
            get {
                return this.dOBField;
            }
            set {
                if ((dOBField.Equals(value) != true)) {
                    this.dOBField = value;
                    this.OnPropertyChanged("DOB");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        public virtual void OnPropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
            if ((handler != null)) {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    public partial class Course : System.ComponentModel.INotifyPropertyChanged {
        
        private string courseNameField;
        
        private string courseIdField;
        
        public string CourseName {
            get {
                return this.courseNameField;
            }
            set {
                if ((this.courseNameField != null)) {
                    if ((courseNameField.Equals(value) != true)) {
                        this.courseNameField = value;
                        this.OnPropertyChanged("CourseName");
                    }
                }
                else {
                    this.courseNameField = value;
                    this.OnPropertyChanged("CourseName");
                }
            }
        }
        
        public string CourseId {
            get {
                return this.courseIdField;
            }
            set {
                if ((this.courseIdField != null)) {
                    if ((courseIdField.Equals(value) != true)) {
                        this.courseIdField = value;
                        this.OnPropertyChanged("CourseId");
                    }
                }
                else {
                    this.courseIdField = value;
                    this.OnPropertyChanged("CourseId");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        public virtual void OnPropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
            if ((handler != null)) {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    public partial class EnrollmentInfo : System.ComponentModel.INotifyPropertyChanged {
        
        private string studentEmailIdField;
        
        private string courseIdField;
        
        public string StudentEmailId {
            get {
                return this.studentEmailIdField;
            }
            set {
                if ((this.studentEmailIdField != null)) {
                    if ((studentEmailIdField.Equals(value) != true)) {
                        this.studentEmailIdField = value;
                        this.OnPropertyChanged("StudentEmailId");
                    }
                }
                else {
                    this.studentEmailIdField = value;
                    this.OnPropertyChanged("StudentEmailId");
                }
            }
        }
        
        public string CourseId {
            get {
                return this.courseIdField;
            }
            set {
                if ((this.courseIdField != null)) {
                    if ((courseIdField.Equals(value) != true)) {
                        this.courseIdField = value;
                        this.OnPropertyChanged("CourseId");
                    }
                }
                else {
                    this.courseIdField = value;
                    this.OnPropertyChanged("CourseId");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        public virtual void OnPropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
            if ((handler != null)) {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    public partial class EnrollmentData : System.ComponentModel.INotifyPropertyChanged {
        
        private Student studentField;
        
        private Course courseField;
        
        public EnrollmentData() {
            this.courseField = new Course();
            this.studentField = new Student();
        }
        
        public Student Student {
            get {
                return this.studentField;
            }
            set {
                if ((this.studentField != null)) {
                    if ((studentField.Equals(value) != true)) {
                        this.studentField = value;
                        this.OnPropertyChanged("Student");
                    }
                }
                else {
                    this.studentField = value;
                    this.OnPropertyChanged("Student");
                }
            }
        }
        
        public Course Course {
            get {
                return this.courseField;
            }
            set {
                if ((this.courseField != null)) {
                    if ((courseField.Equals(value) != true)) {
                        this.courseField = value;
                        this.OnPropertyChanged("Course");
                    }
                }
                else {
                    this.courseField = value;
                    this.OnPropertyChanged("Course");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        public virtual void OnPropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
            if ((handler != null)) {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
