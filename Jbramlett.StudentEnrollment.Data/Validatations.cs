﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Jbramlett.StudentEnrollment.Data
{
    public partial class Student
    {
        public ValidationResponse Validate()
        {
            bool hasErrors = false;
            ValidationResponse response = new ValidationResponse();

            if(!Utilities.ValidateString(FirstName, 100))
            {
                hasErrors = true;
                response.MoreResponses.Add(new ValidationResponse() { Message = "First Name is invalid" });
            }

            if (!Utilities.ValidateString(LastName, 100))
            {
                hasErrors = true;
                response.MoreResponses.Add(new ValidationResponse() { Message = "Last Name is invalid" });
            }

            if (!Utilities.ValidateEmail(EmailId))
            {
                hasErrors = true;
                response.MoreResponses.Add(new ValidationResponse() { Message = "Email is invalid" });
            }

            if (!Utilities.ValidateString(Location, 100, restrict: false))
            {
                hasErrors = true;
                response.MoreResponses.Add(new ValidationResponse() { Message = "Email is invalid" });
            }

            if(DOB >= DateTime.Today || DOB < DateTime.Today - TimeSpan.FromDays(100 * 365))
            {
                hasErrors = true;
                response.MoreResponses.Add(new ValidationResponse() { Message = "DOB is invalid" });
            }

            if (hasErrors)
            {
                response.Message = "Student has validation errors";

                return response;
            }

            return null;
        }
    }
    public partial class Course
    {
        public ValidationResponse Validate()
        {
            bool hasErrors = false;
            ValidationResponse response = new ValidationResponse();

            if (!Utilities.ValidateString(CourseId, 100))
            {
                hasErrors = true;
                response.MoreResponses.Add(new ValidationResponse() { Message = "CourseId is invalid" });
            }

            if (!Utilities.ValidateString(CourseName, 100))
            {
                hasErrors = true;
                response.MoreResponses.Add(new ValidationResponse() { Message = "CourseName is invalid" });
            }


            if (hasErrors)
            {
                response.Message = "Course has validation errors";

                return response;
            }

            return null;
        }
    }
    public partial class EnrollmentInfo
    {
        public ValidationResponse Validate()
        {
            bool hasErrors = false;
            ValidationResponse response = new ValidationResponse();

            if (!Utilities.ValidateString(CourseId, 100))
            {
                hasErrors = true;
                response.MoreResponses.Add(new ValidationResponse() { Message = "CourseId is invalid" });
            }

            if (!Utilities.ValidateEmail(StudentEmailId))
            {
                hasErrors = true;
                response.MoreResponses.Add(new ValidationResponse() { Message = "StudentEmailId is invalid" });
            }


            if (hasErrors)
            {
                response.Message = "EnrollmentInfo has validation errors";

                return response;
            }

            return null;
        }
    }
    public partial class EnrollmentData
    {
        public ValidationResponse Validate()
        {
            bool hasErrors = false;
            ValidationResponse response = new ValidationResponse();

            if(Course == null)
            {
                hasErrors = true;
                response.MoreResponses.Add(new ValidationResponse() { Message = "Course is null" });
            }
            else
            {
                var courseValid = Course.Validate();
                if(courseValid!= null)
                {
                    hasErrors = true;
                    response.MoreResponses.Add(courseValid);
                }
            }

            if (Student == null)
            {
                hasErrors = true;
                response.MoreResponses.Add(new ValidationResponse() { Message = "Student is null" });
            }
            else
            {
                var studentValid = Student.Validate();
                if (studentValid != null)
                {
                    hasErrors = true;
                    response.MoreResponses.Add(studentValid);
                }
            }


            if (hasErrors)
            {
                response.Message = "EnrollmentData has validation errors";

                return response;
            }

            return null;
        }
    }

    public static class Utilities
    {
        private static string EmailRegex = @"^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$";
        
        public static bool ValidateString(string input, int maxLength, bool restrict = true)
        {
            if (string.IsNullOrEmpty(input))
                return false;

            if (input.Length > maxLength)
                return false;

            if (restrict)
            {
                var cArray = input.ToCharArray();
                foreach (var c in cArray)
                {
                    if (!Char.IsLetterOrDigit(c))
                        return false;
                }
            }

            return true;
        }

        public static bool ValidateEmail(string input)
        {
            Regex emailRegEx = new Regex(EmailRegex);
            var isMatch = emailRegEx.IsMatch(input);
            return isMatch;
        }
    }
}
